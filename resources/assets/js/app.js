Dropzone.options.addImages ={
    maxFilesize: 5,
    acceptedFiles: 'image/*',
    success:function(file, response){
            if (file.status == 'success') {
                handleDropzoneFileUpload.handleSuccess(response);
                console.log(response);
            } else {
                handleDropzoneFileUpload.handleError(response);
            }
    }
};

var handleDropzoneFileUpload = {
    handleError: function(response) {
        console.log(response);
    },
    handleSuccess: function(response) {
        var imageList = $('#gallery-images ul');
        var thumbSrc = baseUrl + '/gallery/images/thumbnails/' + response.file_name;
        var imageSrc = baseUrl + '/' + response.file_path;
        $(imageList).append('<li><a href="'+ imageSrc +'" data-lightbox="'+ response.gallery_id +'"><img src="'+ thumbSrc +'"></a></li>');
    }
};
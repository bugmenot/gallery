<!DOCTYPE html>
<html>

<head>

    <title>
        tytuł
    </title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url(elixir('css/all.css')) }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/lightbox.css') }}">

    <script>
        var baseUrl = "{{ url('/') }}"

        @yield('js')
    </script>
    <style>
        @yield('css')
    </style>
</head>

<body>

    <div class="container">
        @if(Session::has('Error'))
            <div class="alert alert-danger">{{ Session::get('Error') }}</div>
        @endif

        @if(Session::has('Success'))
            <div class="alert alert-success">{{ Session::get('Success') }}</div>
        @endif
    </div>

    <div class="container">

        @yield('content')

    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>
    <script type="text/javascript" src="{{ url(elixir('js/all.js')) }}"></script>
    <script type="text/javascript" src="{{ asset('js/lightbox.min.js') }}"></script>
</body>

</html>
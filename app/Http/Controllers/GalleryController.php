<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class GalleryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showList() {
        $galleries = Gallery::where('created_by', Auth::user()->id)->get();
        return view('gallery.gallery')
            ->with('galleries', $galleries);
    }

    public function saveGallery(Request $request) {

        $validator = Validator::make($request->all(),[
        'gallery_name' => 'required|min:3',
        ]);

        if ($validator->fails()){
            return redirect('gallery/list')
                ->withErrors($validator)
                ->withInput();
        }
        $gallery = new Gallery;

        //zapisanie albumu w bazie

        $gallery->name = $request->input('gallery_name');
        $gallery->created_by = Auth::user()->id;
        $gallery->published = 1;
        $gallery->save();

        return redirect()->back();
    }

    public function showGalleryId($id){
        $gallery = Gallery::findOrFail($id);

        return view('gallery.show-gallery')
            ->with('gallery', $gallery);
    }

    public function saveImage(Request $request) {
        //nazwa
        $file = $request->file('file');

        //uniqueID + nazwa
        $filename = uniqid() . $file->getClientOriginalName();

        //przeniesienie pliku
        if(!file_exists('gallery/images')){
            mkdir('gallery/images',0777,true);
        }
        $file->move('gallery/images', $filename);

        //miniaturka
        if(!file_exists('gallery/images/thumbnails')){
            mkdir('gallery/images/thumbnails',0777,true);
        }
        $thumbnail = Image::make('gallery/images/'. $filename)->resize(240, 160)->save('gallery/images/thumbnails/'. $filename, 50);

        //zapisanie w bazie danych
        $gallery = Gallery::find($request->input('gallery_id'));
        $image = $gallery->images()->create([
           'gallery_id'     =>  $request->input('gallery_id'),
           'file_name'      =>  $filename,
           'file_size'      =>  $file->getClientSize(),
           'file_mime'      =>  $file->getClientMimeType(),
           'file_path'      =>  'gallery/images/' . $filename,
           'created_by'     =>  Auth::user()->id
        ]);
        return $image;
    }

    public function deleteGallery($id){

        $currentGallery = Gallery::findOrFail($id);

        if($currentGallery->created_by != Auth::user()->id){
            abort('403', 'Nie masz uprawnień aby usunąć tę galerię');
        }

        $images = $currentGallery->images();

        foreach ($currentGallery->images as $image){
            unlink(public_path($image->file_path));
            unlink(public_path('gallery/images/thumbnails/' . $image->file_name));
        }

        $currentGallery->images()->delete();

        $currentGallery->delete();

        return redirect()->back();
    }
}

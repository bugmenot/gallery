<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
//|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/' , function() {
        if (Auth::check()) {
            return redirect('gallery/list');
        } else {
            return view('user.login');
        }
    });

    Route::post('login' , 'Auth\AuthController@login');

    Route::get('logout' , function() {
        Auth::logout();
        return redirect('/');
    });

    Route::get('gallery/list' , 'GalleryController@showList');
    Route::post('gallery/save' , 'GalleryController@saveGallery');
    Route::get('gallery/view/{id}' , 'GalleryController@showGalleryId');
    Route::post('upload' , 'GalleryController@saveImage');
    Route::get('gallery/delete/{id}', 'GalleryController@deleteGallery');
});
